﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace Yudo.Lib.FileWatch
{
    public class FileWatchService
    {
        FileSystemWatcher watcher;
        string path;
        private Logger _log = LogManager.GetCurrentClassLogger();

        public FileWatchService()
        {
            this.watcher = new FileSystemWatcher();
        }

        public void InicializeWatcher()
        {
            this.path = ConfigurationManager.AppSettings["FileWatchPath"];


            this.watcher.Path = path;
            this.watcher.IncludeSubdirectories = true;
            this.watcher.Created += FileSystemWatcher_Created;
            this.watcher.Renamed += FileSystemWatcher_Renamed;
            this.watcher.Deleted += FileSystemWatcher_Deleted;

            this.watcher.EnableRaisingEvents = true;

            Console.WriteLine("started FileSystemWatcher");
            _log.Info("started FileSystemWatcher");
        }

        private void FileSystemWatcher_Created(object sender, FileSystemEventArgs e)
        {
            string user_name = "unknow";
            try
            {
                user_name = System.IO.File.GetAccessControl(e.FullPath).GetOwner(typeof(System.Security.Principal.NTAccount)).ToString();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                user_name = "error";
            }

            if(e.FullPath.Contains(@"\CAD\Rev"))
            {
                _log.Info(" ChangeType: " + e.ChangeType.ToString() + " FullPath: " + e.FullPath + " Name: " + e.Name + " username: " + user_name);
            }
                
        }

        private void FileSystemWatcher_Renamed(object sender, FileSystemEventArgs e)
        {
            string user_name = "unknow";
            try
            {
                user_name = System.IO.File.GetAccessControl(e.FullPath).GetOwner(typeof(System.Security.Principal.NTAccount)).ToString();
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                user_name = "error";
            }

            if (e.FullPath.Contains(@"\CAD\Rev"))
            {
                _log.Info(" ChangeType: " + e.ChangeType.ToString() + " FullPath: " + e.FullPath + " Name: " + e.Name + " username: " + user_name);
            }
        }

        private void FileSystemWatcher_Deleted(object sender, FileSystemEventArgs e)
        {
            string user_name = "unknow";
            
            //_log.Info(" ChangeType: " + e.ChangeType.ToString() + " FullPath: " + e.FullPath + " Name: " + e.Name + " username: " + user_name);
            if (e.FullPath.Contains(@"\CAD\Rev"))
            {
                _log.Info(" ChangeType: " + e.ChangeType.ToString() + " FullPath: " + e.FullPath + " Name: " + e.Name + " username: " + user_name);
            }
        }

        private void getEmailInfo(string id, string versao)
        {
            string query = @"select quempediu,quemtratou,atribuidoa, tipo,estado,us.email as emailquempediu,a.email as emailquemtratou,b.email as emailatribuidoa
                        from u_pd left join us on u_pd.quempediu = us.username 
                        left join us a on u_pd.quemtratou = a.username 
                        left join us b on u_pd.atribuidoa = b.username 
                        where bostamp in
                        (
                        select bostamp from bo(nolock) where fref = '"+id+"' and ndos in (1,3,40)) and u_pd.versao=" + versao +" and estado in ('ATRIBUIDO','PENDENTE','PENDENTE CLIENTE')";
        }

        private string[] extractInfo(string path)
        {
            // \\server01\Orders-ID\2018\10\201810120001E\YPT18-1357\CAD\Rev0\1-Informação_Cliente\Novo Documento de Texto.txt

            var splitedpath = path.Split('\\');

            string id = splitedpath[6];
            string versao = splitedpath[9].Replace("Rev","");

            if (!string.IsNullOrEmpty(id) && string.IsNullOrEmpty(versao))
            {

                string[] dados = new string[2];

                dados[0] = id;
                dados[1] = versao;

                return dados;
            }
            else return null;
        }

        private bool SendEmailChange(string path, string changeType)
        {
            string[] dados = extractInfo(path);

            var emailinfo = getEmailInfo(dados[0], dados[1]);

            string to = "";
            string subject = "";
            string body = "";

            MailMessage mail = new MailMessage("noreply.yudo@gmail.com", to);
            SmtpClient client = new SmtpClient();
            client.Port = 587;
            client.EnableSsl = true;
            client.Credentials = new System.Net.NetworkCredential("noreply.yudo@gmail.com", "ye9789.CO");
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Host = "smtp.gmail.com";
            mail.Subject = subject;
            mail.Body = body;
            client.Send(mail);

            return true;
        }
    }
}
