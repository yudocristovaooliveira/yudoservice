﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using PetaPoco;
using Yudo.Lib.FileWatch.Models;

namespace Yudo.Lib.FileWatch
{
    public class ChangeYudoStruture
    {
        private Logger _log = LogManager.GetCurrentClassLogger();
        private Database DB { get; set; }

        public ChangeYudoStruture()
        {
            this.DB = new Database("PHCConnection");
        }

        public void CopyComercialProcessToOrdersId()
        {
            _log.Info("starting Process");
            string path = @"\\server01\Comercial-National\Processos\P15\";
            //string[] allfiles = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            string[] allfiles = Directory.GetDirectories(path, "*", SearchOption.TopDirectoryOnly);

            foreach (var item in allfiles)
            {

                string ypt = item.Split('\\')[item.Split('\\').Length-1];
                if(ypt.Length >= 10)
                {
                    var records = DB.Fetch<Proposta>(@"select top 1 obranome,fref,u_estado from bo(nolock) where ndos=3 
                        and obranome like '" + ypt + "%' and fref <> '' order by obranome desc");

                    if (records.Count() > 0)
                    {

                        var p = records.FirstOrDefault().fref;
                        if(!p.StartsWith("SV") && !p.StartsWith("RC") && !p.StartsWith("SP"))
                        {
                            string DestinationPath = @"\\server01\Orders-ID\";
                            //201612160011E
                            string ano = p.Substring(0, 4);
                            string mes = p.Substring(4, 2);

                            DestinationPath += ano + @"\" + mes + @"\" + p;

                            if(Directory.Exists(DestinationPath))
                            {
                                try
                                {
                                    //create root ypt path
                                    DestinationPath = DestinationPath + @"\" + ypt;
                                    if (!Directory.Exists(DestinationPath))
                                    {
                                        Directory.CreateDirectory(DestinationPath);

                                        _log.Info(DestinationPath);
                                        //Now Create all of the directories
                                        foreach (string dirPath in Directory.GetDirectories(item, "*", SearchOption.AllDirectories))
                                            Directory.CreateDirectory(dirPath.Replace(item, DestinationPath));

                                        //Copy all the files & Replaces any files with the same name
                                        foreach (string newPath in Directory.GetFiles(item, "*.*", SearchOption.AllDirectories))
                                            File.Copy(newPath, newPath.Replace(item, DestinationPath), true);

                                        Directory.Delete(item, true);

                                    } else
                                    {
                                        Directory.Delete(item, true);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _log.Error(ex);
                                }
                                
                            }

                        }

                    }

                }

            }
        }

    }
}
