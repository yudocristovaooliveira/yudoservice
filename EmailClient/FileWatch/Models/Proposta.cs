﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.FileWatch.Models
{
    [PetaPoco.TableName("bo")]
    [PetaPoco.PrimaryKey("bostamp")]
    public class Proposta
    {
        public string fref { get; set; }
        public string obranome { get; set; }
        public string u_estado { get; set; }
    }
}
