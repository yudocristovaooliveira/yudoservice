﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.FileWatch.Models
{
    [PetaPoco.TableName("u_pd")]
    [PetaPoco.PrimaryKey("u_pdstamp")]
    class PedidoDesenho
    {
        public string quempediu { get; set; }
        public string quemtratou { get; set; }
        public string atribuidoa { get; set; }
        public string tipo { get; set; }
        public string emailquempediu { get; set; }
        public string emailquemtratou { get; set; }
        public string emailatribuidoa { get; set; }
    }
}
