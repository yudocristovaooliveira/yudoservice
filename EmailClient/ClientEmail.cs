﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using System.Timers;
using ActiveUp.Net.Mail;
using System.Configuration;
using System.IO;
using Yudo.Lib.Models;

namespace YudoService
{
    public enum AccountType { POP, IMAP };

    public class ClientEmail
    {
        NLog.Logger _log = LogManager.GetCurrentClassLogger();

        private Timer timer;
        private Timer timerConnect;

        public string Host { get; set; }
        public string Username { get; set; }
        public string Pass { get; set; }
        public int Port { get; set; }
        public bool Ssl { get; set; }
        public AccountType Tipo { get; set; }
        public int NTentativas { get; set; }
        public List<EmailJob> Trabalhos { get; set; }
        public string TempPath { get; set; }

        public ClientEmail()
        {
            this.timer = new Timer(Int32.Parse(ConfigurationManager.AppSettings["FetchInterval"]));
            this.timerConnect = new Timer(3600000);

            this.Host = ConfigurationManager.AppSettings["Host"];
            this.Username = ConfigurationManager.AppSettings["Username"];
            this.Pass = ConfigurationManager.AppSettings["Pass"];
            this.Port = Int32.Parse(ConfigurationManager.AppSettings["Port"]);
            this.Ssl = bool.Parse(ConfigurationManager.AppSettings["Ssl"]);
            this.TempPath = ConfigurationManager.AppSettings["TempPath"];
            string tipo = ConfigurationManager.AppSettings["AccountType"];
            if (tipo == "POP")
                this.Tipo = AccountType.POP;
            else
                this.Tipo = AccountType.IMAP;

        }

        public void InicializeEmailClient()
        {
            try
            {
                this.timer.Stop();
                this.timerConnect.Stop();
                this.Trabalhos = new List<EmailJob>();
                

                this.timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                this.timerConnect.Elapsed += new ElapsedEventHandler(OnTimedEventConnect);
                this.timer.Start();
                this.timerConnect.Start();
                this.NTentativas = 0;

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        private void OnTimedEventConnect(object sender, EventArgs e)
        {
            try
            {
                _log.Info("Check timer start");
                if (this.TestConnection())
                {
                    //_log.Info("Timer started");
                    this.timer.Start();
                    NTentativas = 0;
                }
                else
                {
                    _log.Info("main timer stopped check failed");
                    this.timer.Stop();
                    NTentativas++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                this.timer.Stop();
            }
        }

        private void OnTimedEvent(object sender, EventArgs e)
        {

            _log.Info("Main timer start");
            try
            {
                this.Trabalhos = new List<EmailJob>();
                this.timer.Stop();
                if (NTentativas <= 3)
                {
                    try
                    {
                        if (this.Tipo == AccountType.IMAP)
                            this.fetchLast5MessagesImap();
                        else
                            this.fetchLast5MessagesPop();

                        TreatMailJobs();

                    }
                    catch (Exception ex)
                    {
                        _log.Info("----ERROR-----");
                        _log.Info(ex);
                    }
                    this.timer.Start();
                }
            }
            catch (Exception ex)
            {

                _log.Info("----ERROR-----");
                _log.Info(ex);
            }
 
        }

        #region test connections

        public bool TestConnection()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.Host) && !string.IsNullOrEmpty(this.Username)
                    && this.Port > 0)
                {
                    switch (this.Tipo)
                    {
                        case AccountType.POP:
                            return testePopConnection();
                        case AccountType.IMAP:
                            return testeImapConnection();
                        default:
                            return testeImapConnection();
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _log.Info("----ERROR-----");
                _log.Info(ex);
                return false;
            }
        }

        private bool testePopConnection()
        {
            try
            {
                Pop3Client pop3 = new Pop3Client();
                if (this.Ssl)
                    pop3.ConnectSsl(this.Host, this.Port, this.Username, this.Pass);
                else
                    pop3.Connect(this.Host, this.Port, this.Username, this.Pass);

                if (pop3.IsConnected)
                {
                    int contagem = pop3.MessageCount;

                    pop3.Disconnect();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return false;
            }
        }

        private bool testeImapConnection()
        {
            try
            {
                Imap4Client imap4 = new Imap4Client();
                if (this.Ssl)
                {
                        imap4.ConnectSsl(this.Host, this.Port);
                }
                else
                    imap4.Connect(this.Host, this.Port);

                if (imap4.IsConnected)
                {
                    string autenticate = imap4.Login(this.Username, this.Pass);
                    Mailbox mailbox = imap4.SelectMailbox("INBOX");
                    int contagem = mailbox.MessageCount;

                    imap4.Disconnect();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                return false;
            }
        }

        #endregion

        public void fetchLast5MessagesImap()
        {

            Imap4Client imap4 = new Imap4Client();
            if (this.Ssl)
            {
                imap4.ConnectSsl(this.Host, this.Port);
            }
            else
                imap4.Connect(this.Host, this.Port);

            string autenticate = imap4.Login(this.Username, this.Pass);
            //imap4.Connect("imap.gmail.com",993, "cristovao.oliveira@alidata.com.pt", "cristo12085");
            Mailbox mailbox = imap4.SelectMailbox("INBOX");
            int contagem = mailbox.MessageCount;
            Fetch fetcha = mailbox.Fetch;
            ActiveUp.Net.Mail.Message message;
            //int[] ids = inbox.Search("UNSEEN");

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    if (contagem > 0)
                    {
                        EmailJob j = new EmailJob();

                        message = fetcha.MessageObject(contagem);                        
                        if (message.Attachments.Count > 0)
                        {
                            try
                            {
                                if (Directory.Exists(this.TempPath))
                                {

                                    message.Attachments.StoreToFolder(this.TempPath);
                                    foreach (MimePart item in message.Attachments)
                                    {
                                        j.Attachments.Add(item.Filename);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                
                            }
                        }
                        j.Subject = message.Subject;
                        j.Body = message.BodyHtml.Text;

                        j.From = message.From.Email;


                        if (message.Cc != null)
                        {
                            if (message.Cc.Count > 0)
                            {
                                foreach (var item in message.Cc)
                                {
                                    j.Cc += item.Email + ";";
                                }
                            }
                        }

                        if (message.To != null)
                        {
                            if (message.To.Count > 0)
                            {
                                foreach (var item in message.To)
                                {
                                    j.To += item.Email + ";";
                                }
                            }
                        }

                        Console.WriteLine("id:" + i + " " + j.Subject);
                        _log.Info("id:" + i + " " + j.Subject);

                       
                         mailbox.DeleteMessage(contagem, true);
                       
                        contagem--;

                        this.Trabalhos.Add(j);
                    }
                    else
                        break;
                }
                catch (Exception ex)
                {
                    if (ex is Imap4Exception)
                    {
                        _log.Info("----ERROR-----");
                        _log.Info(ex);
                        this.NTentativas++;
                    }
                    else
                    {
                        _log.Info("----ERROR-----");
                        _log.Info(ex);
                        this.NTentativas++;
                    }
                }

            }
            imap4.Disconnect();
        }

        public void fetchLast5MessagesPop()
        {
            Pop3Client pop3 = new Pop3Client();
            string connect = string.Empty;
            if (this.Ssl)
                connect = pop3.ConnectSsl(this.Host, this.Port, this.Username, this.Pass);
            else
                connect = pop3.Connect(this.Host, this.Port, this.Username, this.Pass);

            int contagem = pop3.MessageCount;

            for (int i = 0; i < 5; i++)
            {
                try
                {
                    if (contagem > 0)
                    {

                        EmailJob j = new EmailJob();
                        Message message = null;
                        message = pop3.RetrieveMessageObject(contagem);
                        if (message.Attachments.Count > 0)
                        {
                            message.Attachments.StoreToFolder(this.TempPath);
                            foreach (MimePart item in message.Attachments)
                            {
                                j.Attachments.Add(item.Filename);
                            }
                        }
                        j.Subject = message.Subject;
                        j.Body = message.BodyHtml.Text;
                        j.From = message.From.Email;

                        Console.WriteLine("id:" + i + " " + j.Subject);
                        _log.Info("id:" + i + " " + j.Subject);

                        pop3.DeleteMessage(contagem);                        

                        contagem--;

                        this.Trabalhos.Add(j);
                    }
                }
                catch (Exception ex)
                {
                    _log.Info("----ERROR-----");
                    _log.Info(ex);
                    this.NTentativas++;
                }
            }

            pop3.Disconnect();
        }

        public void TreatMailJobs()
        {
            foreach (var item in this.Trabalhos)
            {

                try
                {

                    if (item.Subject.Contains("(") && item.Subject.Contains(")"))
                    {

                        string fref = item.Subject.Split('(')[1].Split(')')[0].Trim();

                        if(!string.IsNullOrEmpty(fref))
                        {
                            PetaPoco.Database db = new PetaPoco.Database("PHCConnection");
                            var result = db.Fetch<dynamic>("select qncstamp from qnc where cod = @0",fref);

                            if(result.Count() > 0)
                            {
                                EmailCom newRecord = new EmailCom();
                                newRecord.u_emailcomstamp = Yudo.Lib.Utils.Configs.NewPHCStamp("CFO");
                                newRecord.qncstamp = result.FirstOrDefault().qncstamp;
                                newRecord.bodyq = string.IsNullOrEmpty(item.Body) ? " " : item.Body;
                                newRecord.ccq = string.IsNullOrEmpty(item.Cc) ? " " : item.Cc;
                                newRecord.fromq = string.IsNullOrEmpty(item.From) ? " " : item.From;
                                newRecord.subjectq = string.IsNullOrEmpty(item.Subject) ? " " : item.Subject;
                                newRecord.toq = string.IsNullOrEmpty(item.To) ? " " : item.To;
                                newRecord.ousrhora = DateTime.Now.ToShortTimeString();                                

                                
                                foreach (var ficheiro in item.Attachments)
                                {
                                    try
                                    {
                                        newRecord.anexosq += ficheiro + ";";

                                        string caminho = this.TempPath + "\\" + ficheiro;

                                        _log.Info("Attachments File received: " + ficheiro);
                                        string extension = Path.GetExtension(ficheiro);
                                        //string hashedFileName = DateTime.Now.ToFileTime().ToString() + extension;

                                        try
                                        {
                                            File.Move(caminho, this.TempPath + "\\" + ficheiro);
                                        }
                                        catch (Exception ex)
                                        {
                                            _log.Error(ex);
                                        }

                                        System.Threading.Thread.Sleep(1000);

                                    }
                                    catch (Exception ex)
                                    {
                                        _log.Error(ex);
                                    }
                                }

                                if (string.IsNullOrEmpty(newRecord.anexosq)) newRecord.anexosq = " ";

                                db.Insert(newRecord);

                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                }
            }
        }

    }
}
