﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.ArmarioSeco.Models
{
    [PetaPoco.TableName("u_if_phc_h")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class Phc_h
    {
        public int id { get; set; }
        public short tipo_input { get; set; }
        public string tipo_string { get; set; }
        public DateTime data { get; set; }
        public string fref { get; set; }
        public decimal peno { get; set; }        
        public string dpto { get; set; }
        public string resumo { get; set; }
        public decimal no { get; set; }
        public decimal estab { get; set; }
       
    }
}
