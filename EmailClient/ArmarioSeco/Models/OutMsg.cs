﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.ArmarioSeco.Models
{
    [PetaPoco.TableName("OutMsg")]
    [PetaPoco.PrimaryKey("OutMsgID", AutoIncrement = true)]
    public class OutMsg
    {
        public int OutMsgID { get; set; }
        public string MsgName { get; set; }
        public int FromID { get; set; }
        public int ToID { get; set; }
        public DateTime MsgDateTime { get; set; }
        public string Login { get; set; }
        public string LoginName { get; set; }
        public string Processed { get; set; }
        public string Message { get; set; }
    }
}
