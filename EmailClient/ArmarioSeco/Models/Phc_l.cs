﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.ArmarioSeco.Models
{
    [PetaPoco.TableName("u_if_phc_l")]
    [PetaPoco.PrimaryKey("id", AutoIncrement = true)]
    public class Phc_l
    {
        public int id { get; set; }
        public int h_id { get; set; }
        public DateTime data { get; set; }
        public string fref { get; set; }
        public decimal peno { get; set; }
        public string Ref { get; set; }
        public string design { get; set; }
        public decimal qtt { get; set; }
        public decimal ptno { get; set; }
        public string obs { get; set; }
        public string cct { get; set; }
        public string nome { get; set; }
        public string unidade { get; set; }
        public decimal punit { get; set; }    
        public string wip { get; set; }

    }
}
