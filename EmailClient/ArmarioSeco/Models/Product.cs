﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.ArmarioSeco.Models
{
    [PetaPoco.TableName("Product")]
    [PetaPoco.PrimaryKey("ServerProductID", AutoIncrement = true)]
    public class Product
    {
        public int ServerProductID { get; set; }
        public int ServerVendorID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PartNumber { get; set; }
        public string VendorName { get; set; }
        public int Qtd { get; set; }
    
    }
}
