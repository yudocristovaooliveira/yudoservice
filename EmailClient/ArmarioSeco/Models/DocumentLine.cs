﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.ArmarioSeco.Models
{
    public class DocumentLine
    {
        public Product p { get; set; }
        public int qtd { get; set; }
        public string ccusto { get; set; }
        public string LoginName { get; set; }
        public DateTime msgDate { get; set; }
        public int msgId { get; set; }
    }
}
