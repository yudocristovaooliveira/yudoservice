﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using NLog;
using Yudo.Lib.ArmarioSeco.Models;
using System.IO;
using System.Net.Mail;
using System.Timers;
using System.Configuration;

namespace Yudo.Lib.ArmarioSeco
{
    public class SecoService
    {
        private static int ProductIdPosition = 5;
        private static int QtdPosition = 8;
        private static short InterfaceId = 9;
        private static short InterfaceTransferenciaId = 11;
        private static int CentroCusto = 11;

        public int LastIdCheck { get; set; }
        private Database DB { get; set; }
        private Logger _log = LogManager.GetCurrentClassLogger();
        private string FilePath { get; set; }
        private Timer timer;

        public SecoService()
        {

            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            this.DB = new Database("SecoConnection");

            this.FilePath = System.AppDomain.CurrentDomain.BaseDirectory + "OutMsgId.txt";

            if (!File.Exists(this.FilePath))
            {
                using (FileStream fs = File.Create(this.FilePath))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes("1");
                    fs.Write(info, 0, info.Length);
                }
            }

            string fileContent = File.ReadAllText(this.FilePath);

            if(!string.IsNullOrEmpty(fileContent))
                this.LastIdCheck = Int32.Parse(fileContent);

            this.timer = new Timer(Int32.Parse(ConfigurationManager.AppSettings["FetchIntervalSECO"]));

        }

        public void InicializeSecoService()
        {
            try
            {
                this.timer.Stop();
                this.timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                this.timer.Start();

            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        private void OnTimedEvent(object sender, EventArgs e)
        {

            _log.Info("Main SECO timer start");
            try
            {
                this.timer.Stop();
                
                try
                {
                    CheckSecoChanges();

                }
                catch (Exception ex)
                {
                    _log.Info("----ERROR-----");
                    _log.Info(ex);
                }
                this.timer.Start();
                
            }
            catch (Exception ex)
            {

                _log.Info("----ERROR-----");
                _log.Info(ex);
            }

        }

        public void UpdateCheckedId()
        {
            string fileContent = File.ReadAllText(this.FilePath);

            if (!string.IsNullOrEmpty(fileContent))
                this.LastIdCheck = Int32.Parse(fileContent);
        }


        public void CheckSecoChanges()
        {

            UpdateCheckedId();

            try
            {
                //get take requests
                var records = DB.Fetch<OutMsg>(@"select top 200 OutMsg.*,
                                    (CabinetUser.FirstName + ' '+CabinetUser.Lastname) as LoginName
                                    from OutMsg 
                                    left join CabinetUser on CabinetUser.Login = OutMsg.Login
                                    where Msgname in ('SPMsg.ISPPocketRefill','SPMsg.ISPPocketTakeEx') and OutMsgID > @0
                                    order by OutMsgID", this.LastIdCheck);

                List<DocumentLine> produtosConsumidos = new List<DocumentLine>();
                List<DocumentLine> produtosColocados = new List<DocumentLine>();


                if (records.Count > 0)
                {
                    int outId = 0;

                    foreach (OutMsg item in records)
                    {

                        string[] dataArray = item.Message.Split('|');
                        if(dataArray.Length > 11)
                        {
                            //|SPMsg.ISPPocketTakeEx|90|515562|1002|100750351|88|88|10|1|100004135|010604|UserID|100608822|
                            //SPMsg.ISPPocketReturnEx|127|527111|1026|100750391|18|18|1|Y|1|100004135|010606|UserID|101059397|
                            //Login|DoorNumber|pocketnumber|ServerproductId|m_ExpectedQty|m_ActualQty|m_TakeQty|ServerAllocCodeId|AllocValue|UserId|ServerUserID      
                            //|SPMsg.ISPPocketRefill|98|527111|1034|100661337|4|4|10|0|UserID|100612807|
                            DocumentLine docline = new DocumentLine();

                            string ProductId = dataArray[ProductIdPosition];
                            string qtdTaken = dataArray[QtdPosition];
                            string ccusto = dataArray[CentroCusto];                            
                            

                            Product p = this.DB.FirstOrDefault<Product>("select * from Product where ServerProductID = @0", ProductId);                        
                            docline.p = p;
                            docline.qtd = Int32.Parse(qtdTaken);
                            docline.LoginName = item.LoginName;
                            docline.msgDate = item.MsgDateTime;
                            docline.ccusto = ccusto;
                            docline.msgId = item.OutMsgID;

                            if (item.MsgName.Equals("SPMsg.ISPPocketRefill"))
                            {                              
                                docline.ccusto = dataArray[CentroCusto + 1];
                                produtosColocados.Add(docline);                               
                            } else
                            {
                                produtosConsumidos.Add(docline);
                            }
                            outId = item.OutMsgID;

                            
                            string info = string.Format("Product: {0} PartNumber: {1} Qtd: {2} Date {3} User: {4}", p.Name, p.PartNumber, qtdTaken, item.MsgDateTime, item.LoginName);
                            _log.Info(info);
                        }
                    }

                    if (produtosColocados.Count > 0)
                    {
                        //string infoEmail = string.Format("Product: <b>{0}</b><br>PartNumber: <b>{1}</b><br>Qtd: <b>{2}</b><br>Date <b>{3}</b><br>User: <b>{4}</b>", p.Name, p.PartNumber, qtdTaken, item.MsgDateTime, item.LoginName);
                        //mandar email
                        //SendEmail(infoEmail);
                        InsereTransArmazemPHC(produtosColocados);
                    }

                    //inserir consumo interno
                    if (produtosConsumidos.Count > 0)
                    {
                        //string infoEmail = string.Format("Product: <b>{0}</b><br>PartNumber: <b>{1}</b><br>Qtd: <b>{2}</b><br>Date <b>{3}</b><br>User: <b>{4}</b>", p.Name, p.PartNumber, qtdTaken, item.MsgDateTime, item.LoginName);
                        //mandar email
                        //SendEmail(infoEmail);
                        InsereConsumoInternoPHC(produtosConsumidos);
                    }



                    File.WriteAllText(this.FilePath, outId.ToString(), Encoding.UTF8);

                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        public void SendEmail(string body)
        {
            try
            {
                MailMessage mail = new MailMessage("cristovao.oliveira@yudoeu.com", "aires.gaio@yudoeu.com");
                SmtpClient client = new SmtpClient();
                client.Port = 587; //465
                client.EnableSsl = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("cristovao.oliveira@yudoeu.com", "ye9789.CO");
                client.Host = "mail.yudoeu.com";
                mail.Subject = "Movimento máquina SECO";
                mail.IsBodyHtml = true;
                mail.Body = "<p>"+body+"</p>";                
                mail.CC.Add("cristovao.oliveira@yudoeu.com");
                client.Send(mail);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        public bool InsereConsumoInternoPHC(OutMsg m, Product p, int qtd, string ccusto)
        {
            bool resultado = true;
            try
            {
                Database dbPHC = new Database("PHCConnection");
                
                using (var transaction = dbPHC.GetTransaction())
                {
                    // Some transactional DB work
                    Phc_h header = new Phc_h();
                    header.tipo_input = InterfaceId;
                    header.tipo_string = "Consumo Interno - Alertas";
                    header.fref = "STOCK";
                    header.resumo = "Cutting tools";
                    header.data = m.MsgDateTime;
                    header.peno = 1; //yudo eu
                    header.no = 1;
                    header.estab = 0;

                    var idHeader = dbPHC.Insert(header);
                    if (idHeader != null)
                    {
                        Phc_l line = new Phc_l();
                        line.h_id = Int32.Parse(idHeader.ToString());
                        line.data = header.data;
                        line.Ref = p.PartNumber;
                        line.qtt = decimal.Parse(qtd.ToString());
                        line.peno = header.peno;
                        line.obs = p.Name.Length > 40 ? p.Name.Substring(0,40) : p.Name;
                        line.design = m.LoginName;
                        line.cct = ccusto;
                        dbPHC.Insert(line);
                    }

                    transaction.Complete();
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                resultado = false;
            }
            return resultado;
        }

        public bool InsereConsumoInternoPHC(List<DocumentLine> lista)
        {
            bool resultado = true;
            try
            {
                Database dbPHC = new Database("PHCConnection");

                using (var transaction = dbPHC.GetTransaction())
                {
                    // Some transactional DB work
                    Phc_h header = new Phc_h();
                    header.tipo_input = InterfaceId;
                    header.tipo_string = "Consumo Interno - Alertas";
                    header.fref = "STOCK";
                    header.resumo = "Cutting tools";
                    header.data = DateTime.Now;
                    header.peno = 1; //yudo eu
                    header.no = 1;
                    header.estab = 0;

                    var idHeader = dbPHC.Insert(header);
                    if (idHeader != null)
                    {

                        foreach (var item in lista)
                        {
                            Phc_l line = new Phc_l();
                            line.h_id = Int32.Parse(idHeader.ToString());
                            line.data = header.data;
                            line.Ref = item.p.PartNumber;
                            line.qtt = decimal.Parse(item.qtd.ToString());
                            line.peno = header.peno;
                            line.data = item.msgDate;
                            line.obs = item.p.Name.Length > 40 ? item.p.Name.Substring(0, 40) : item.p.Name;
                            line.design = item.LoginName;
                            line.cct = item.ccusto;
                            line.wip = item.msgId.ToString();
                            line.ptno = 61;
                            dbPHC.Insert(line);

                        }                    
                    }
                    transaction.Complete();
                    _log.Info("InsereConsumoInternoPHC call with " + lista.Count() + " lines");
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                resultado = false;
            }
            return resultado;
        }

        public bool InsereTransArmazemPHC(List<DocumentLine> lista)
        {
            bool resultado = true;
            try
            {
                Database dbPHC = new Database("PHCConnection");

                using (var transaction = dbPHC.GetTransaction())
                {
                    // Some transactional DB work
                    Phc_h header = new Phc_h();
                    header.tipo_input = InterfaceTransferenciaId;
                    header.tipo_string = "Trans. Armazém - Alertas";
                    header.fref = "STOCK";
                    header.resumo = "Cutting tools";
                    header.data = DateTime.Now;
                    header.peno = 1; //yudo eu
                    header.no = 1;
                    header.estab = 0;

                    var idHeader = dbPHC.Insert(header);
                    if (idHeader != null)
                    {

                        foreach (var item in lista)
                        {
                            Phc_l line = new Phc_l();
                            line.h_id = Int32.Parse(idHeader.ToString());
                            line.data = header.data;
                            line.Ref = item.p.PartNumber;
                            line.qtt = decimal.Parse(item.qtd.ToString());
                            line.peno = header.peno;
                            line.data = item.msgDate;
                            line.obs = item.p.Name.Length > 40 ? item.p.Name.Substring(0, 40) : item.p.Name;
                            line.design = item.LoginName;
                            line.cct = item.ccusto;
                            line.ptno = 6;
                            line.punit = 61;
                            dbPHC.Insert(line);

                        }
                    }
                    transaction.Complete();
                    _log.Info("InsereTransArmazemPHC call with " + lista.Count() + " lines");
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex);
                resultado = false;
            }
            return resultado;
        }

    }
}
