﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.Models
{
    public class EmailJob
    {
        public string From { get; set; }
        public string Cc { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<string> Attachments { get; set; }

        public EmailJob()
        {
            this.Attachments = new List<string>();
        }

    }
}
