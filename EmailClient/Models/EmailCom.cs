﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.Models
{
    [PetaPoco.TableName("u_emailcom")]
    [PetaPoco.PrimaryKey("u_emailcomstamp",AutoIncrement = false)]
    public class EmailCom
    {
        public string u_emailcomstamp { get; set; }

        public string qncstamp { get; set; }

        public string subjectq { get; set; }

        public string fromq { get; set; }

        public string ccq { get; set; }

        public string toq { get; set; }

        public string bodyq { get; set; }

        public string ousrhora { get; set; }

        public string anexosq { get; set; }

    }
}
