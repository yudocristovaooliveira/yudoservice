﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yudo.Lib.Utils
{
    public class Configs
    {
        public static string NewPHCStamp(string iniciais)
        {
            string semiStamp = Guid.NewGuid().ToString().Substring(0, 12).Replace("-", "").ToUpper() + "," + Guid.NewGuid().ToString().Substring(0, 10).Replace("-","").ToUpper();
            return iniciais.Substring(0,3) + semiStamp;
        }
    }
}
