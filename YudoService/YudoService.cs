﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Yudo.Lib.ArmarioSeco;
using Yudo.Lib.FileWatch;

namespace YudoService
{
    public partial class YudoService : ServiceBase
    {

        Logger _log = LogManager.GetCurrentClassLogger();


        public YudoService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            ClientEmail c = new ClientEmail();
            c.InicializeEmailClient();

            SecoService s = new SecoService();
            s.InicializeSecoService();

            FileWatchService sc = new FileWatchService();
            sc.InicializeWatcher();
        }

        protected override void OnStop()
        {

        }
    }
}
