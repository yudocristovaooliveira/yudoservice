﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YudoService;
using Yudo.Lib.ArmarioSeco;
using Yudo.Lib.FileWatch;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory);

            //ClientEmail c = new ClientEmail();
            //c.InicializeEmailClient();

            //SecoService sc = new SecoService();
            //sc.CheckSecoChanges();

            //FileWatchService sc = new FileWatchService();
            //sc.InicializeWatcher();
            ChangeYudoStruture sc = new ChangeYudoStruture();
            sc.CopyComercialProcessToOrdersId();

            Console.WriteLine("waiting for key");
            Console.Read();
        }
    }
}
